PhotobucketScraper
==================
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://paypal.me/michelvh)

Last June, Photobucket quietly updated its terms of service. As a result, 3rd-party hosting of images is no longer
allowed in the free or cheaper plans, only in the $399.99 a year plan. If you used Photobucket to host the images for 
your blog, the original images are no longer visible. Instead, a "ransom" image requesting you to upgrade your account
is visible.

While Photobucket provides a way to download the original images, it is a time-consuming task to search, download and
replace all images on your blog manually. PhotobucketScraper is a script to download all images to your server in the
original folder structure.

If you use WordPress, the script can automatically replace all images for you.

If you use other software, you must replace the images yourself. Feel free to create an issue on the
[issue tracker](https://bitbucket.org/michelvh/photobucketscraper/issues). It might be possible to support your
software. Please include the output of the script in your issue.

How to Use
----------
To use PhotobucketScraper, you must have shell access to your server. If you don't have shell access, or aren't
familiar with using shell access to run a PHP script, you should wait for the web version of PhotobucketScraper.

1. Create a folder (i.e. `pbs`) on your server
2. Upload the files to the folder
3. Copy the file `pbs_config.php.dist` to `pbs_config.php`
4. Update the [configuration](#markdown-header-how-to-use-configuration) in `pbs_config.php`
5. Execute the script: `php pbs.php`
 
The script will try to login to your Photobucket account, create a list of albums, and download all images in all your
albums. After downloading all images, the script will automatically replace all images if you use WordPress and have
configured the script correctly.

**Important:** the script requires at least one album. If you don't have albums in your account, please create an album
before running the script.

How to Use (Configuration)
--------------------------
**Example configuration**

````php
<?php

// The username of your Photobucket account
define('PB_USERNAME', 'michelvh');

// The password of your Photobucket account
define('PB_PASSWORD', 'l3tm31n!');

// The path to your Wordpress folder, relative to the Photobucket scraper
define('WP_DIR', false);

// The path to the download folder, relative to the web root
define('TARGET_PATH', 'library');

// The path to the download folder
if (WP_DIR) {
    define('TARGET_DIR', WP_DIR.'/'.TARGET_PATH);
} else {
    define('TARGET_DIR', __DIR__.'/'.TARGET_PATH);
}
````

PhotobucketScraper will download your images to the `library` subfolder in the `pbs` folder.

**Example configuration with WordPress**

This assumes you have created the `pbs` folder as a subfolder of the WordPress folder.

````php
<?php

// The username of your Photobucket account
define('PB_USERNAME', 'michelvh');

// The password of your Photobucket account
define('PB_PASSWORD', 'l3tm31n!');

// The path to your WordPress folder, relative to the Photobucket scraper
define('WP_DIR', '../');

// The path to the download folder, relative to the web root
define('TARGET_PATH', 'wp-content/uploads-pb');

// The path to the download folder
if (WP_DIR) {
    define('TARGET_DIR', WP_DIR.'/'.TARGET_PATH);
} else {
    define('TARGET_DIR', __DIR__.'/'.TARGET_PATH);
}
````

PhotobucketScraper will download your images to the `wp-content/uploads-pb` subfolder in the WordPress folder, and
replace all images in your posts and comments.

Questions
---------
If you have any questions, feel free to open an issue on the [issue tracker](https://bitbucket.org/michelvh/photobucketscraper/issues).

License
-------
PhotobucketScraper is released under the [MIT License](http://www.opensource.org/licenses/MIT).
