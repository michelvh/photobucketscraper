<?php

// Include functions
require __DIR__.'/functions.php';

// Check configuration
if (!file_exists(__DIR__.'/pbs_config.php')) {
    die('Config not found, did you forget to create it?');
}

// Include configuration
require __DIR__.'/pbs_config.php';

// Execute
logout();
login();

$albums = getAlbums();
if (count($albums)) {
    array_unshift($albums, getRootAlbum($albums[0]));

    foreach ($albums as $album) {
        $images = getImagesInAlbum($album);
        foreach ($images as $image) {
            saveImage($image);
        }
    }

    $pbHost = PB_HOST;
    $pbHostImages = preg_replace('#s(\d{4})\.#', 'i$1.', $pbHost);
    $searchUrl = removeSlashes($pbHostImages.'/'.$albums[0]['path'].'/');

    if (WP_DIR) {
        replaceImagesWordpress($searchUrl);
    } else {
        replaceImages($searchUrl);
    }
} else {
    printf("No albums found!\n");
    printf("Please create an album and run the script again.\n");
}
