<?php

/**
 * Logout to Photobucket
 */
function logout()
{
    printf("Logout to Photobucket...\n");

    file_put_contents(__DIR__.'/cookies.txt', '');
}

/**
 * Login to Photobucket
 */
function login()
{
    printf("Login to Photobucket...\n");

    $request = getRequest('https://secure.photobucket.com/action/auth/login', [
        'username' => PB_USERNAME,
        'password' => PB_PASSWORD,
    ]);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        'Referer: https://secure.photobucket.com/login',
    ]);
    curl_setopt($request, CURLOPT_HEADER, true);
    $response = curl_exec($request);
    $requestInfo = curl_getinfo($request);
    curl_close($request);

    if (!strstr($requestInfo['url'], '?postlogin=true')) {
        file_put_contents(__DIR__.'/pbs_login.html', $response);
        die(sprintf("Login to Photobucket failed"));
    }

    $host = str_replace('?postlogin=true', '', $requestInfo['url']);
    define('PB_HOST', $host);
}

/**
 * Gets all albums from Photobucket
 */
function getAlbums()
{
    printf("Get albums...\n");

    $request = getRequest(PB_HOST.'/component/Albums-SubalbumList?json=1');
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        'X-Requested-With: XMLHttpRequest',
    ]);
    $response = curl_exec($request);
    curl_close($request);

    $albumsData = json_decode($response, true);
    $albums = getAlbumsFromData($albumsData['body']['subAlbums']);

    return $albums;
}

/**
 * Gets all albums from Photobucket data
 *
 * @param array $albumsData
 * @return array
 */
function getAlbumsFromData(array $albumsData)
{
    $albums = [];

    foreach ($albumsData as $albumData) {
        $subAlbumsData = [];
        if (isset($albumData['subAlbums'])) {
            $subAlbumsData = $albumData['subAlbums'];
            unset($albumData['subAlbums']);
        }

        $albums[] = $albumData;
        $albums = array_merge($albums, getAlbumsFromData($subAlbumsData));
    }

    return $albums;
}

/**
 * Gets the root album
 *
 * @param array $album
 * @return array
 */
function getRootAlbum(array $album)
{
    $replaceValue = function($value) use ($album){
        return preg_replace('#/'.preg_quote($album['location'], '#').'$#', '', $value);
    };

    $rootAlbum = [
        'linkUrl' => $replaceValue($album['linkUrl']),
        'path' => $replaceValue($album['path']),
        'url' => $replaceValue($album['url']),
        'location' => '',
        'title' => '(Your Bucket)',
    ];

    return $rootAlbum;
}

/**
 * Gets all images in an album from Photobucket
 *
 * @param array $album
 * @param int $page
 * @return array
 */
function getImagesInAlbum(array $album, $page = 1)
{
    printf("Get images in album '%s' (page %d)...\n", $album['title'], $page);

    $request = getRequest(PB_HOST.'/component/Common-PageCollection-Album-AlbumPageCollection?json=1&filters[album]='.urlencode($album['path']).'&limit=23&page='.$page);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        'X-Requested-With: XMLHttpRequest',
    ]);
    $response = curl_exec($request);
    curl_close($request);

    $imagesData = json_decode($response, true);
    $images = $imagesData['body']['objects'];

    if (($imagesData['body']['currentOffset'] + $imagesData['body']['pageSize']) < $imagesData['body']['total']) {
        $images = array_merge($images, getImagesInAlbum($album, $page+1));
    }

    return $images;
}

/**
 * Gets an image from Photobucket
 *
 * @param array $image
 * @param string
 */
function getImage($image)
{
    $request = getRequest($image['fullsizeUrl']);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        'Referer: '.$image['linkUrl'],
    ]);
    $response = curl_exec($request);
    curl_close($request);

    return $response;
}

/**
 * Saves an image from Photobucket
 */
function saveImage(array $image)
{
    printf("Save image '%s' in '%s'...\n", $image['name'], $image['location']);

    $targetDir = TARGET_DIR.'/'.$image['location'];
    $targetFile = $targetDir.'/'.$image['name'];

    // Create directory
    if (!file_exists($targetDir)) {
        mkdir($targetDir, 0777, true);
    }

    // Create file
    file_put_contents($targetFile, getImage($image));
}

/**
 * Replaces the image from Photobucket with the saved images in Wordpress
 */
function replaceImagesWordpress($searchUrl)
{
    require WP_DIR.'/wp-config.php';

    $replaceUrl = removeSlashes(get_option('siteurl').'/'.TARGET_PATH.'/');

    printf("Replacing images in Wordpress posts...\n");
    $wpdb->query(sprintf(
        'UPDATE %s SET post_content = REPLACE(post_content, "%s", "%s")',
        $wpdb->posts,
        $searchUrl,
        $replaceUrl
    ));

    printf("Replacing images in Wordpress comments...\n");
    $wpdb->query(sprintf(
        'UPDATE %s SET comment_content = REPLACE(comment_content, "%s", "%s")',
        $wpdb->comments,
        $searchUrl,
        $replaceUrl
    ));

    printf("If you have images in your sidebar, update your sidebar content manually:\n");
    printf("Search: %s\n", $searchUrl);
    printf("Replace: %s\n", $replaceUrl);
}

/**
 * Replaces the image from Photobucket with the saved images
 */
function replaceImages($searchUrl)
{
    $replaceUrl = removeSlashes('http://<domain>/<path>/'.TARGET_PATH.'/');

    printf("Update your content manually:\n");
    printf("Search: %s\n", $searchUrl);
    printf("Replace: %s\n", $replaceUrl);
}

/**
 * Get a request
 *
 * @param string $url
 * @param array $data
 * @return resource
 */
function getRequest($url, $data = null)
{
    $request = curl_init($url);

    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($request, CURLOPT_COOKIEFILE, __DIR__.'/cookies.txt');
    curl_setopt($request, CURLOPT_COOKIEJAR, __DIR__.'/cookies.txt');
    curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

    if ($data !== null) {
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $data);
    }

    return $request;
}

/**
 * @param string $str
 * @return string
 */
function removeSlashes($str) {
    return preg_replace('#([^:])//+#', '$1/', $str);
};
